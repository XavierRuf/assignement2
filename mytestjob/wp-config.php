<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'mytestjob' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';CgT`|1:xNSmL!{WU<ioM`;v?Y`ch@$_CyZO7=rOBPwNsc.56+*]iaOM{t9/*1tW' );
define( 'SECURE_AUTH_KEY',  'S6;)I=9B=%reu_;:l]~}wVjk@{.fz/84tX3I+#C/,waGjAns,J_.9l!(izf2yo=d' );
define( 'LOGGED_IN_KEY',    '9v<<7](O)b<Q7;o?6dpNm:r*&#V+@3P!CP<%kU/Dmw`Hjp<l/YWRNUH+&p(<28bU' );
define( 'NONCE_KEY',        'b4fBmBZ^2A=@#[D%}sY`paNgquC3w6l+QoSuZVPyw|8vqbPa;q}JgSDcyZ{Siz9,' );
define( 'AUTH_SALT',        'uC!vbBB/|zh^54y:)#r:8vIgfxBoG+5zqtuaw=uaZ(-Ju^HLCIC!f/0GQWlh-4GQ' );
define( 'SECURE_AUTH_SALT', '/{o3cbyy|ZnJ)T8Ub1(pC2s?58+dlcY^Af&IjVA:sP&Y^ibh.7Nh4t0~b_692V}}' );
define( 'LOGGED_IN_SALT',   '}k[8*.VV$+$B}KRM-zuXNY>ib)Zcow5DscswV+v *42u]qz5 .sm3D2gt.6i6u81' );
define( 'NONCE_SALT',       '_=wrsx-WhH8BW>a>2%m~Cu@S@(C-@/(DU/gzsG#&LtKr1DamROceCRQ+rxMb_w7&' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'job_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
