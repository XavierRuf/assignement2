<?php

function sportify_scripts() {

    wp_enqueue_style( 'sportify-gfonts', 'http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700');
    wp_enqueue_style( 'sportify-icons', get_template_directory_uri() . '/vendor/simple-line-icons/css/simple-line-icons.css' );
    wp_enqueue_style( 'sportify-bootstrap', get_template_directory_uri() . '/vendor/bootstrap/css/bootstrap.min.css' );
    wp_enqueue_style( 'sportify-animate', get_template_directory_uri() . '/css/animate.css' );
    wp_enqueue_style( 'sportify-swiper', get_template_directory_uri() . '/vendor/swiper/css/swiper.min.css' );

    wp_enqueue_style( 'sportify-layout', get_stylesheet_uri());


    wp_enqueue_script( 'jquery', get_template_directory_uri() . '/vendor/jquery.min.js', array(), '20151215', true );

    wp_enqueue_script( 'jquery-migrate', get_template_directory_uri() . '/vendor/jquery-migrate.min.js', array(jquery), '20151215', true );

    wp_enqueue_script( 'bootstrapJS', get_template_directory_uri() . '/vendor/bootstrap/js/bootstrap.min.js', array(), '20151215', true );

    wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/vendor/jquery.easing.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'jquery-back-to-top', get_template_directory_uri() . '/vendor/jquery.back-to-top.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'jquery-smooth-scroll', get_template_directory_uri() . '/vendor/jquery.smooth-scroll.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'jquery-wow', get_template_directory_uri() . '/vendor/jquery.wow.min.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'jquery-parallax', get_template_directory_uri() . '/vendor/jquery.parallax.min.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'jquery-appear', get_template_directory_uri() . '/vendor/jquery.appear.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'jquery-swiper', get_template_directory_uri() . '/vendor/swiper/js/swiper.jquery.min.js', array(jquery), '20151215', true );
    
    wp_enqueue_script( 'js-layout', get_template_directory_uri() . '/vendor/swiper/js/swiper.jquery.min.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'js-progress', get_template_directory_uri() . '/js/progress-bar.min.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'js-swiper', get_template_directory_uri() . '/js/swiper.min.js', array(jquery), '20151215', true );
    wp_enqueue_script( 'js-wow', get_template_directory_uri() . '/js/wow.min.js', array(jquery), '20151215', true );
    
}
add_action( 'wp_enqueue_scripts', 'sportify_scripts' );

