<footer class="footer">
            <div class="content container">
                <div class="row">
                    <div class="col-xs-6">
                        <img class="footer-logo" src="<?php echo get_template_directory_uri()?>/img/logo.png" alt="Acecv Logo">
                    </div>
                </div>
                <!--// end row -->
            </div>
        </footer>
<?php wp_footer(); ?>

</body>
</html>
